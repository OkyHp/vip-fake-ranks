#pragma semicolon 1
#pragma newdecls required

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <clientprefs>
#include <vip_core>

int			m_iCompetitiveRanking,
			g_iPlayerRanks[MAXPLAYERS+1],
			g_iVipStatus[MAXPLAYERS+1];
char		g_sRankType[MAXPLAYERS+1][64];
KeyValues	g_hConfig;
Handle		g_hCookie[2];

static const char g_sFeature[][] = {"FakeRanks", "FakeRanksMenu"};

enum
{
	RANK_TYPE = 0,
	RANK_ID
}

public Plugin myinfo =
{
	name	=	"VIP Tab Fake Ranks",
	author	=	"OkyHp",
	version	=	"1.0.1",
	url		=	"https://blackflash.ru/, https://dev-source.ru/, https://hlmod.ru/"
};

public void OnPluginStart()
{
	if(GetEngineVersion() != Engine_CSGO)
	{
		SetFailState("This plugin works only on CS:GO");
	}

	m_iCompetitiveRanking = FindSendPropInfo("CCSPlayerResource", "m_iCompetitiveRanking");

	g_hCookie[RANK_TYPE] = RegClientCookie("VIP_TabFakeRanks_Type", "Ranks type for vip VIP_TabFakeRanks", CookieAccess_Private);
	g_hCookie[RANK_ID] = RegClientCookie("VIP_TabFakeRanks_Rank", "Ranks id for vip VIP_TabFakeRanks", CookieAccess_Private);

	LoadTranslations("VIP_TabFakeRanks.phrases");

	if(VIP_IsVIPLoaded())
	{
		VIP_OnVIPLoaded();
	}
}

public void OnMapStart()
{
	SDKHook(FindEntityByClassname(-1, "cs_player_manager"), SDKHook_ThinkPost, OnThinkPost);

	if (g_hConfig)
	{
		delete g_hConfig;
	}

	char szPath[256];
	g_hConfig = new KeyValues("Config");
	BuildPath(Path_SM, szPath, sizeof(szPath), "data/vip/modules/fake_ranks.ini");

	if(g_hConfig.ImportFromFile(szPath))
	{
		g_hConfig.Rewind();
		if (g_hConfig.GotoFirstSubKey())
		{
			char szBuffer[256];
			do {
				if (g_hConfig.GotoFirstSubKey(false))
				{
					do {
						FormatEx(szBuffer, sizeof(szBuffer), "materials/panorama/images/icons/skillgroups/skillgroup%i.svg", g_hConfig.GetNum(NULL_STRING));
						if(FileExists(szBuffer))
						{
							AddFileToDownloadsTable(szBuffer);
						}
					} while (g_hConfig.GotoNextKey(false));
					g_hConfig.GoBack();
				}
			} while (g_hConfig.GotoNextKey());
		}
	}
}

public void OnClientCookiesCached(int iClient)
{
	GetClientCookie(iClient, g_hCookie[RANK_TYPE], g_sRankType[iClient], sizeof(g_sRankType[]));
	if (!g_sRankType[iClient][0] && g_hConfig)
	{
		g_hConfig.Rewind();
		if (g_hConfig.GotoFirstSubKey())
		{
			g_hConfig.GetSectionName(g_sRankType[iClient], sizeof(g_sRankType[]));
		}
	}

	char szRank[12];
	GetClientCookie(iClient, g_hCookie[RANK_ID], szRank, sizeof(szRank));
	g_iPlayerRanks[iClient] = StringToInt(szRank);
}

public void VIP_OnVIPLoaded()
{
	VIP_RegisterFeature(g_sFeature[0],	BOOL,		TOGGLABLE,	_,				OnDisplayItem,	_,			DISABLED);
	VIP_RegisterFeature(g_sFeature[1],	VIP_NULL,	SELECTABLE,	OnItemSelect,	OnDisplayItem,	OnItemDraw);
}

public void OnPluginEnd()
{
	if(CanTestFeatures() && GetFeatureStatus(FeatureType_Native, "VIP_UnregisterFeature") == FeatureStatus_Available)
	{
		VIP_UnregisterFeature(g_sFeature[0]);
		VIP_UnregisterFeature(g_sFeature[1]);
	}
}

public void VIP_OnVIPClientLoaded(int iClient)
{
	g_iVipStatus[iClient] = view_as<int>(VIP_GetClientFeatureStatus(iClient, g_sFeature[0]));
}

public void VIP_OnVIPClientAdded(int iClient, int iAdmin)
{
	g_iVipStatus[iClient] = view_as<int>(VIP_GetClientFeatureStatus(iClient, g_sFeature[0]));
}

public void OnClientDisconnect(int iClient)
{
	g_iVipStatus[iClient] = 0;
}

public Action VIP_OnFeatureToggle(int iClient, const char[] szFeature, VIP_ToggleState eOldStatus, VIP_ToggleState &eNewStatus)
{
	if(!strcmp(szFeature, g_sFeature[0], false)) 
	{
		g_iVipStatus[iClient] = view_as<int>(eNewStatus);
	}
}

public bool OnDisplayItem(int iClient, const char[] szFeature, char[] szDisplay, int iMaxLength)
{
	if (!strcmp(szFeature, g_sFeature[1]))
	{
		FormatEx(szDisplay, iMaxLength, "%T", szFeature, iClient);
		return true;
	}

	static const char szState[][] = {"Disabled", "Enabled", "No_Access"};
	SetGlobalTransTarget(iClient);
	FormatEx(szDisplay, iMaxLength, "%t [%t]", szFeature, szState[g_iVipStatus[iClient]]);

	return true;
}

public int OnItemDraw(int iClient, const char[] szFeature, int iStyle)
{
	switch(g_iVipStatus[iClient])
	{
		case ENABLED: return ITEMDRAW_DEFAULT;
		case DISABLED: return ITEMDRAW_DISABLED;
		case NO_ACCESS: return ITEMDRAW_RAWLINE;
	}
	return iStyle;
}

public bool OnItemSelect(int iClient, const char[] szFeature)
{
	SetGlobalTransTarget(iClient);
	Menu hMenu = new Menu(Handler_FakeRankMenu);
	hMenu.SetTitle("[ %t ]\n ", "FakeRankMenuTitle");

	char szBuffer[128];
	if (TranslationPhraseExists(g_sRankType[iClient]))
	{
		FormatEx(szBuffer, sizeof(szBuffer), "%t [%t]", "SelectTypeRanks", g_sRankType[iClient]);
	}
	else
	{
		FormatEx(szBuffer, sizeof(szBuffer), "%t [%s]", "SelectTypeRanks", g_sRankType[iClient]);
	}
	hMenu.AddItem(NULL_STRING, szBuffer);

	if (g_hConfig)
	{
		g_hConfig.Rewind();
		if (g_hConfig.JumpToKey(g_sRankType[iClient]) && g_hConfig.GotoFirstSubKey(false))
		{
			char	szRank[12],
					szName[64];
			do {
				g_hConfig.GetString(NULL_STRING, szRank, sizeof(szRank));
				g_hConfig.GetSectionName(szName, sizeof(szName));

				if (TranslationPhraseExists(szName))
				{
					char szBufferTr[128];
					FormatEx(szBufferTr, sizeof(szBufferTr), "%t", szName);
					hMenu.AddItem(szRank, szBufferTr);
					continue;
				}
				hMenu.AddItem(szRank, szName);
			} while (g_hConfig.GotoNextKey(false));
		}
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

public int Handler_FakeRankMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch(action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel:
		{
			if(iItem == MenuCancel_ExitBack)
			{
				VIP_SendClientVIPMenu(iClient);
			}
		}
		case MenuAction_Select:
		{
			if (!iItem)
			{
				SelectTypeRanks(iClient);
				return 0;
			}

			char	szBuffer[128],
					szRank[12];
			hMenu.GetItem(iItem, szRank, sizeof(szRank), _, szBuffer, sizeof(szBuffer));
			g_iPlayerRanks[iClient] = StringToInt(szRank);
			SetClientCookie(iClient, g_hCookie[RANK_ID], szRank);
			PrintToChat(iClient, " \x04[ \x02VIP \x04] \x01%t: \x04%s", "SelectedFakeRank", szBuffer);

			OnItemSelect(iClient, NULL_STRING);
		}
	}
	return 0;
}

void SelectTypeRanks(int iClient)
{
	SetGlobalTransTarget(iClient);
	Menu hMenu = new Menu(Handler_SelectTypeRanksMenu);
	hMenu.SetTitle("[ %t ]\n ", "SelectTypeRanks");

	if (g_hConfig)
	{
		g_hConfig.Rewind();
		if (g_hConfig.GotoFirstSubKey())
		{
			char szBuffer[64];
			do {
				g_hConfig.GetSectionName(szBuffer, sizeof(szBuffer));
				if (TranslationPhraseExists(szBuffer))
				{
					char szBufferTr[128];
					FormatEx(szBufferTr, sizeof(szBufferTr), "%t", szBuffer);
					hMenu.AddItem(szBuffer, szBufferTr);
					continue;
				}
				hMenu.AddItem(szBuffer, szBuffer);
			} while (g_hConfig.GotoNextKey());
		}
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

public int Handler_SelectTypeRanksMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch(action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel:
		{
			if(iItem == MenuCancel_ExitBack)
			{
				OnItemSelect(iClient, NULL_STRING);
			}
		}
		case MenuAction_Select:
		{
			char szBuffer[128];
			hMenu.GetItem(iItem, g_sRankType[iClient], sizeof(g_sRankType[]), _, szBuffer, sizeof(szBuffer));
			PrintToChat(iClient, " \x04[ \x02VIP \x04] \x01%t: \x04%s", "SelectedFakeRankType", szBuffer);
			SetClientCookie(iClient, g_hCookie[RANK_TYPE], g_sRankType[iClient]);

			//SelectTypeRanks(iClient);
			OnItemSelect(iClient, NULL_STRING);
		}
	}
}

public void OnThinkPost(int iEntity)
{
	for (int i = 1; i <= MaxClients; ++i)
	{
		if(IsClientInGame(i) && !IsFakeClient(i) && g_iVipStatus[i] == 1)
		{
			SetEntData(iEntity, m_iCompetitiveRanking + i * 4, g_iPlayerRanks[i]);
		}
	}
}

public void OnPlayerRunCmdPost(int iClient, int iButtons)
{
	static int iOldButtons[MAXPLAYERS+1];

	if(iButtons & IN_SCORE && !(iOldButtons[iClient] & IN_SCORE))
	{
		StartMessageOne("ServerRankRevealAll", iClient, USERMSG_BLOCKHOOKS);
		EndMessage();
	}

	iOldButtons[iClient] = iButtons;
}
